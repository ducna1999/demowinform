﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Dtos
{
    public class DichVuDto
    {
        public int Id { get; set; }
        public string TenDichVu { get; set; }
        public string MaDichVu { get; set; }
        public DateTime NgayTao { get; set; }
    }
}
