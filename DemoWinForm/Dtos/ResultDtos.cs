﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Dtos
{
    public class ResultDtos<T>
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public T Model { get; set; }
    }
}
