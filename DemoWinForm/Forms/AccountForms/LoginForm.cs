﻿using DemoWinForm.Dtos;
using DemoWinForm.Services;
using DemoWinForm.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoWinForm.Forms.AccountForms
{
    public partial class LoginForm : Form
    {
        private AccountService accountService;
        Thread th;
        public LoginForm()
        {
            InitializeComponent();

            if (Properties.Settings.Default.SavePassword)
            {
                txtUserName.Text = Properties.Settings.Default.UserName;
                txtPassword.Text = Properties.Settings.Default.Password;
                cbSavePass.Checked = true;
            }
        }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            accountService = new AccountService();

            string userName = txtUserName.Text;
            string password = txtPassword.Text;

            var res = await accountService.Login(userName, password);

            if (res.Status == true)
            {
                if (cbSavePass.Checked)
                {
                    Properties.Settings.Default.UserName = txtUserName.Text;
                    Properties.Settings.Default.Password = txtPassword.Text;
                    Properties.Settings.Default.SavePassword = true;
                    Properties.Settings.Default.Save();
                }

                this.Close();
                th = new Thread(OpenNewForm);
                th.SetApartmentState(ApartmentState.STA);
                th.Start();
            }
            else
                MessageBox.Show(res.Message);
        }

        public void OpenNewForm(Object obj)
        {
            Application.Run(new HomePageForm());
        }
    }
}
