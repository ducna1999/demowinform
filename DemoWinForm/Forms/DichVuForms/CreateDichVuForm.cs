﻿using DemoWinForm.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoWinForm.Forms.DichVuForms
{
    public partial class CreateDichVuForm : Form
    {
        private DichVuService dichVuService;
        DichVuListForm dichVuListForm;
        public CreateDichVuForm(DichVuListForm parent)
        {
            InitializeComponent();
            dichVuListForm = parent;
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            dichVuService = new DichVuService();
            string maDichVu = txtMaDichVu.Text;
            string tenDichVu = txtTenDichVu.Text;

            if(string.IsNullOrEmpty(maDichVu))
            {
                MessageBox.Show("Mã dịch vụ không được để trống");
                return;
            }

            if (string.IsNullOrEmpty(tenDichVu))
            {
                MessageBox.Show("Tên dịch vụ không được để trống");
                return;
            }

            var result = await dichVuService.CreateDichVu(maDichVu, tenDichVu);

            MessageBox.Show(result.Message);

            if(result.Status == true)
            {
                this.Close();
                dichVuListForm.LoadData();
            }
        }
    }
}
