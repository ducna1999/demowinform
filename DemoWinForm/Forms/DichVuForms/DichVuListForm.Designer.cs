﻿namespace DemoWinForm.Forms.DichVuForms
{
    partial class DichVuListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblMaDichVu = new System.Windows.Forms.Label();
            this.txtMaDichVuQry = new System.Windows.Forms.TextBox();
            this.txtTenDichVuQry = new System.Windows.Forms.TextBox();
            this.lblTenDichVuQry = new System.Windows.Forms.Label();
            this.lblNgayTaoQry = new System.Windows.Forms.Label();
            this.dtFromDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtToDate = new System.Windows.Forms.DateTimePicker();
            this.dtgDichVu = new System.Windows.Forms.DataGridView();
            this.btnInsertDichVu = new System.Windows.Forms.Button();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.maDichVu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenDichVu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayTao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dtgDichVu)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMaDichVu
            // 
            this.lblMaDichVu.AutoSize = true;
            this.lblMaDichVu.Location = new System.Drawing.Point(13, 13);
            this.lblMaDichVu.Name = "lblMaDichVu";
            this.lblMaDichVu.Size = new System.Drawing.Size(60, 13);
            this.lblMaDichVu.TabIndex = 0;
            this.lblMaDichVu.Text = "Mã dịch vụ";
            // 
            // txtMaDichVuQry
            // 
            this.txtMaDichVuQry.Location = new System.Drawing.Point(80, 13);
            this.txtMaDichVuQry.Name = "txtMaDichVuQry";
            this.txtMaDichVuQry.Size = new System.Drawing.Size(130, 20);
            this.txtMaDichVuQry.TabIndex = 1;
            this.txtMaDichVuQry.TextChanged += new System.EventHandler(this.QueryData);
            // 
            // txtTenDichVuQry
            // 
            this.txtTenDichVuQry.Location = new System.Drawing.Point(298, 13);
            this.txtTenDichVuQry.Name = "txtTenDichVuQry";
            this.txtTenDichVuQry.Size = new System.Drawing.Size(130, 20);
            this.txtTenDichVuQry.TabIndex = 3;
            // 
            // lblTenDichVuQry
            // 
            this.lblTenDichVuQry.AutoSize = true;
            this.lblTenDichVuQry.Location = new System.Drawing.Point(231, 13);
            this.lblTenDichVuQry.Name = "lblTenDichVuQry";
            this.lblTenDichVuQry.Size = new System.Drawing.Size(64, 13);
            this.lblTenDichVuQry.TabIndex = 2;
            this.lblTenDichVuQry.Text = "Tên dịch vụ";
            // 
            // lblNgayTaoQry
            // 
            this.lblNgayTaoQry.AutoSize = true;
            this.lblNgayTaoQry.Location = new System.Drawing.Point(458, 13);
            this.lblNgayTaoQry.Name = "lblNgayTaoQry";
            this.lblNgayTaoQry.Size = new System.Drawing.Size(50, 13);
            this.lblNgayTaoQry.TabIndex = 4;
            this.lblNgayTaoQry.Text = "Ngày tạo";
            // 
            // dtFromDate
            // 
            this.dtFromDate.CustomFormat = "dd-MM-yyyy";
            this.dtFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFromDate.Location = new System.Drawing.Point(514, 13);
            this.dtFromDate.Name = "dtFromDate";
            this.dtFromDate.Size = new System.Drawing.Size(86, 20);
            this.dtFromDate.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(606, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "~";
            // 
            // dtToDate
            // 
            this.dtToDate.CustomFormat = "dd-MM-yyyy";
            this.dtToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtToDate.Location = new System.Drawing.Point(626, 13);
            this.dtToDate.Name = "dtToDate";
            this.dtToDate.Size = new System.Drawing.Size(86, 20);
            this.dtToDate.TabIndex = 8;
            // 
            // dtgDichVu
            // 
            this.dtgDichVu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgDichVu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgDichVu.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.maDichVu,
            this.tenDichVu,
            this.NgayTao});
            this.dtgDichVu.Location = new System.Drawing.Point(16, 83);
            this.dtgDichVu.Name = "dtgDichVu";
            this.dtgDichVu.Size = new System.Drawing.Size(772, 364);
            this.dtgDichVu.TabIndex = 9;
            // 
            // btnInsertDichVu
            // 
            this.btnInsertDichVu.Location = new System.Drawing.Point(13, 39);
            this.btnInsertDichVu.Name = "btnInsertDichVu";
            this.btnInsertDichVu.Size = new System.Drawing.Size(105, 38);
            this.btnInsertDichVu.TabIndex = 10;
            this.btnInsertDichVu.Text = "Thêm mới dịch vụ";
            this.btnInsertDichVu.UseVisualStyleBackColor = true;
            this.btnInsertDichVu.Click += new System.EventHandler(this.btnInsertDichVu_Click);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // maDichVu
            // 
            this.maDichVu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.maDichVu.DataPropertyName = "maDichVu";
            this.maDichVu.HeaderText = "Mã dịch vụ";
            this.maDichVu.Name = "maDichVu";
            // 
            // tenDichVu
            // 
            this.tenDichVu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tenDichVu.DataPropertyName = "tenDichVu";
            this.tenDichVu.HeaderText = "Tên dịch vụ";
            this.tenDichVu.Name = "tenDichVu";
            // 
            // NgayTao
            // 
            this.NgayTao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NgayTao.DataPropertyName = "NgayTao";
            dataGridViewCellStyle2.Format = "dd/MM/yyyy";
            this.NgayTao.DefaultCellStyle = dataGridViewCellStyle2;
            this.NgayTao.HeaderText = "Ngày tạo";
            this.NgayTao.Name = "NgayTao";
            this.NgayTao.ReadOnly = true;
            // 
            // DichVuListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 459);
            this.ControlBox = false;
            this.Controls.Add(this.btnInsertDichVu);
            this.Controls.Add(this.dtgDichVu);
            this.Controls.Add(this.dtToDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtFromDate);
            this.Controls.Add(this.lblNgayTaoQry);
            this.Controls.Add(this.txtTenDichVuQry);
            this.Controls.Add(this.lblTenDichVuQry);
            this.Controls.Add(this.txtMaDichVuQry);
            this.Controls.Add(this.lblMaDichVu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DichVuListForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dtgDichVu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMaDichVu;
        private System.Windows.Forms.TextBox txtMaDichVuQry;
        private System.Windows.Forms.TextBox txtTenDichVuQry;
        private System.Windows.Forms.Label lblTenDichVuQry;
        private System.Windows.Forms.Label lblNgayTaoQry;
        private System.Windows.Forms.DateTimePicker dtFromDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtToDate;
        private System.Windows.Forms.DataGridView dtgDichVu;
        private System.Windows.Forms.Button btnInsertDichVu;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn maDichVu;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenDichVu;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayTao;
    }
}