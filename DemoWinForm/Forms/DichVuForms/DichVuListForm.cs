﻿using DemoWinForm.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoWinForm.Forms.DichVuForms
{
    public partial class DichVuListForm : Form
    {
        private DichVuService dichVuService;
        public DichVuListForm()
        {
            InitializeComponent();
            dichVuService = new DichVuService();
            var dichVuList = dichVuService.GetDataByCondition();
            dtgDichVu.DataSource = dichVuList;
        }

        public void LoadData()
        {
            var dichVuList = dichVuService.GetDataByCondition();
            dtgDichVu.DataSource = dichVuList;
        }

        public void QueryData()
        {
            string maDichVu = txtMaDichVuQry.Text;
            string tenDichVu = txtTenDichVuQry.Text;
            DateTime fromDate = dtFromDate.Value;
            DateTime toDate = dtToDate.Value;
            var dichVuList = dichVuService.GetDataByCondition(maDichVu,tenDichVu,fromDate,toDate);
            dtgDichVu.DataSource = dichVuList;
        }

        private void btnInsertDichVu_Click(object sender, EventArgs e)
        {
            CreateDichVuForm createDichVuForm = new CreateDichVuForm(this);
            createDichVuForm.Show();
        }

        private void QueryData(object sender, EventArgs e)
        {
            string maDichVu = txtMaDichVuQry.Text;
            string tenDichVu = txtTenDichVuQry.Text;
            DateTime fromDate = dtFromDate.Value;
            DateTime toDate = dtToDate.Value;
            var dichVuList = dichVuService.GetDataByCondition(maDichVu, tenDichVu, fromDate, toDate);
            dtgDichVu.DataSource = dichVuList;
        }
    }
}
