﻿using DemoWinForm.Forms.DichVuForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoWinForm.Forms
{
    public partial class HomePageForm : Form
    {
        public HomePageForm()
        {
            InitializeComponent();
        }

        private void nhaCungCapToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void dichVuToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DichVuListForm dichVuListForm = new DichVuListForm();
            dichVuListForm.TopLevel = false;
            pnlForm.Controls.Add(dichVuListForm);
            dichVuListForm.BringToFront();
            dichVuListForm.Show();
        }
    }
}
