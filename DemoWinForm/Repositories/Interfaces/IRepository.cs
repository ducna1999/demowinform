﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Repositories.Interfaces
{
    public interface IRepository<TEntity>
    {
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(int id);
        IQueryable<TEntity> GetQuery();
        TEntity Insert(TEntity entity);
        Task Update(TEntity entity);
        TEntity Delete(TEntity entity);
        Task SaveChangeAsync();
    }
}
