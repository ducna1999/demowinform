﻿using DemoWinForm.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        public TEntity Delete(TEntity entity)
        {
            using (demoEntities db = new demoEntities())
            {
                return db.Set<TEntity>().Remove(entity);
            }
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            using (demoEntities db = new demoEntities())
            {
                return await db.Set<TEntity>().ToListAsync();
            }
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            using (demoEntities db = new demoEntities())
            {
                return await db.Set<TEntity>().FindAsync(id);
            }
        }

        public IQueryable<TEntity> GetQuery()
        {
            using (demoEntities db = new demoEntities())
            {
                return db.Set<TEntity>();
            }
        }

        public TEntity Insert(TEntity entity)
        {
            using (demoEntities db = new demoEntities())
            {
                //return db.Set<TEntity>().Add(entity);
                var result = db.Set<TEntity>().Add(entity);
                db.SaveChanges();
                return result;
            }
        }

        public async Task SaveChangeAsync()
        {
            using (demoEntities db = new demoEntities())
            {
                await db.SaveChangesAsync();
            }
        }

        public async Task Update(TEntity entity)
        {
            using (demoEntities db = new demoEntities())
            {
               //db.Entry(entity).State = EntityState.Modified;
                db.Set<TEntity>().AddOrUpdate(entity);                                                                  
            }
        }
    }
}
