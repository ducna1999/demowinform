//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DemoWinForm
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sequence
    {
        public string maSeq { get; set; }
        public string format { get; set; }
        public string suffix { get; set; }
        public int soHienTai { get; set; }
        public int soBuocNhay { get; set; }
        public int ngtao { get; set; }
        public bool is_delete { get; set; }
        public System.DateTime ntao { get; set; }
    }
}
