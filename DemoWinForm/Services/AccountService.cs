﻿using DemoWinForm.Dtos;
using DemoWinForm.Services.Interface;
using DemoWinForm.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Services
{
    public class AccountService : IAccountService
    {
        public async Task<ResultDtos<UserDto>> Login(string username, string password)
        {
            ResultDtos<UserDto> result = new ResultDtos<UserDto>();

            using (demoEntities db = new demoEntities())
            {
                using (SHA256 sha256 = SHA256.Create())
                {
                    //hash pasword
                    byte[] hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
                    string hashedPassword = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

                    var user = db.Users.FirstOrDefault(p => p.username == username && p.password == hashedPassword);

                    if (user == null)
                    {
                        result.Status = false;
                        result.Message = "Sai tên đăng nhập hoặc mật khẩu";
                        return result;
                    }

                    //Lưu lại thông tin người đăng nhập
                    LoginInfo.Id = user.id; LoginInfo.Username = username; LoginInfo.Password = password;

                    result.Status = true;
                    result.Message = "Đăng nhập thành công";
                    return result;
                }
            }
        }
    }
}
