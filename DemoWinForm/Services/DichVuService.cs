﻿using DemoWinForm.Dtos;
using DemoWinForm.Repositories;
using DemoWinForm.Services.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Services
{
    public class DichVuService : IDichVuService
    {
        private Repository<DichVu> repository;
        private demoEntities db;
        public async Task<ResultDtos<DichVuDto>> CreateDichVu(string maDichVu, string tenDichVu)
        {
            ResultDtos<DichVuDto> result = new ResultDtos<DichVuDto>();
            //repository = new Repository<DichVu>();
            db = new demoEntities();

            #region validate dữ liệu
            var dichVuCheck = db.DichVus.FirstOrDefault(p=>p.maDichVu == maDichVu);
            if (dichVuCheck != null)
            {
                result.Message = "Mã dịch vụ đã tồn tại";
                result.Status = false;
                return result;
            }
            #endregion

            try
            {
                //var entity = repository.Insert(new DichVu
                //{
                //    tenDichVu = tenDichVu,
                //    maDichVu = maDichVu,
                //    ntao = DateTime.Now
                //});

                var entity = db.DichVus.Add(new DichVu()
                {
                    tenDichVu = tenDichVu,
                    maDichVu = maDichVu,
                    ntao = DateTime.Now
                });
                await db.SaveChangesAsync();

                result.Message = "Thêm mới dịch vụ thành công";
                result.Status = true;
                result.Model = new DichVuDto()
                {
                    MaDichVu = entity.maDichVu,
                    TenDichVu = entity.tenDichVu,
                    NgayTao = entity.ntao
                };

            }
            catch (Exception ex)
            {
                result.Message = "Thêm mới dịch vụ không thành công";
                result.Status = false;
            }

            db.Dispose();

            return result;
        }

        public List<DichVuDto> GetDataByCondition(string maDichVu = null, string tenDichVu = null, DateTime? fromDate = null, DateTime? toDate = null)
        {
            List<DichVuDto> result = new List<DichVuDto>();

            using (demoEntities db = new demoEntities())
            {
                result = (from a in db.DichVus
                          where (maDichVu == null || a.maDichVu.ToLower().Contains(maDichVu.ToLower()))
                          && (tenDichVu == null || a.tenDichVu.ToLower().Contains(tenDichVu.ToLower()))
                          && (fromDate == null || a.ntao >= fromDate)
                          && (toDate == null || a.ntao <= toDate)
                          select new DichVuDto()
                          {
                              Id = a.id,
                              MaDichVu = a.maDichVu,
                              TenDichVu = a.tenDichVu,
                              NgayTao = a.ntao,
                          }).ToList();
            }
            return result;
        }
    }
}
