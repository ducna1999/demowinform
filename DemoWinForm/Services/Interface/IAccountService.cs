﻿using DemoWinForm.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Services.Interface
{
    public interface IAccountService
    {
        Task<ResultDtos<UserDto>> Login(string username, string password);
    }
}
