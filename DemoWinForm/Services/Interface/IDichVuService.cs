﻿using DemoWinForm.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Services.Interface
{
    public interface IDichVuService
    {
        List<DichVuDto> GetDataByCondition(string maDichVu = null, string tenDichVu = null, DateTime? fromDate = null, DateTime? toDate = null);

        Task<ResultDtos<DichVuDto>> CreateDichVu(string maDichVu, string tenDichVu);
    }
}
