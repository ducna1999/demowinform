//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DemoWinForm
{
    using System;
    using System.Collections.Generic;
    
    public partial class TyGia
    {
        public int id { get; set; }
        public System.DateTime ngayDLieu { get; set; }
        public string maTienTe { get; set; }
        public decimal giaMua { get; set; }
        public decimal giaTraoDoi { get; set; }
        public decimal giaBan { get; set; }
    }
}
