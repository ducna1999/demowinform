﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoWinForm.Util
{
    public class FormCommand
    {
        public static void OpenNewForm(Form form)
        {
            Application.Run(form);
        }
    }
}
