﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWinForm.Util
{
    public static class LoginInfo
    {
        public static int Id { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
    }
}
